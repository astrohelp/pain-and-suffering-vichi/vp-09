module ODU_solver
    use params
    implicit none
    
    private
    public :: RungeKutta
    public :: Extr_adams
    public :: Inter_adams
    public :: ia
    public :: ea
    public :: rk
    
    contains
    
    subroutine Write_to_file(file, T, X)
    real(8), dimension(0:, 1:), intent(in) :: X
    real(8), dimension(0:), intent(in) :: T
    character(6), intent(in) :: file

        open(10, file = file)
            do i = 0, interval_end/h + 1
                write(10,*) t(i), X(i,:)
            enddo
        close(10)
        
    end subroutine Write_to_file
    
    subroutine RungeKutta(X)
    real(8), dimension(0:, 1:), intent(out) :: X
    real(8), dimension(0:size(X)/ndim-1) :: T
    integer(4) :: i
    character(6) :: file
        
        T = 0; X(0,:) = X_started
        do i = 1, size(x)/ndim -1
            X(i,:) = rk(X(i-1,:), t(i-1))
            T(i) = h*i
        enddo
    
        file = 'rk.dat'
        call Write_to_file(file, T, X)
        
    end subroutine RungeKutta
    
    function rk(x0, t) result(x)
    real(8), dimension(1:), intent(in) :: x0
    real(8), intent(in) :: t
    real(8), dimension(1:ndim) :: x
    real(8), dimension(1:ndim) :: k1, k2, k3, k4


        k1 = h*f(t, x0)
        k2 = h*f(t+h/2, x0+k1/2)
        k3 = h*f(t+h/2, x0+k2/2)
        k4 = h*f(t+h, x0+k3)
        
        x = x0 + (k1 + 2*k2 + 2*k3 + k4)/6.0
    
    end function rk
    
    subroutine Extr_adams(X)
    real(8), dimension(0:, 1:), intent(out) :: X
    real(8), dimension(0:size(X)/ndim-1) :: T
    real(8), dimension(0:n_extr_adams-1) :: A
    real(8), dimension(1:ndim) :: sum
    integer(4) :: i, j
    character(6) :: file
    
        T = 0; X(0,:) = X_started
        do i = 0, n_extr_adams-2
            X(i+1,:) = rk(X(i,:), t(i))
            T(i+1) = h*(i+1)
        enddo
        
        A = A_adams(n_extr_adams,(/(i,i=0,n_extr_adams-1)/))
        
        do i = n_extr_adams-1, size(x)/ndim - 2
            call ea(X, T, A, i)
        enddo
    
        file = 'ea.dat'
        call Write_to_file(file, T, X)
        
    end subroutine Extr_adams
    
    subroutine ea(X, T, A, i)
    real(8), dimension(0:,1:) :: X
    real(8), dimension(0:) :: T
    real(8), dimension(0:) :: A
    real(8), dimension(1:ndim) :: sum
    integer(4) :: i, j
    
        sum = 0
        do j = 0, n_extr_adams-1
            sum = sum + A(j)*f(t(i-j),X(i-j,:))
        enddo
        X(i+1,:) = X(i,:) + h*sum
        T(i+1) = h*(i+1)
    
    end subroutine ea
    
    elemental function A_adams(n,j)
    integer(4), intent(in) :: n, j
    integer(4) :: i
    real(8) :: A_adams

        A_adams = (-1.0)**j / product((/(i,i=1,j)/))/product((/(i,i=1,n-1-j)/)) * Integral_A(n, j)

    end function A_adams

    pure function integral_A(n, j)
    integer(4), intent(in) :: n, j
    integer(4) :: a,i
    real(8), dimension(0:n-1) :: z, helpler
    real(8) :: integral_A

        z = 0; z(0) = 1;
        do a=0, n-1
            if (a .ne. j) then
                helpler = z*a
                z = eoshift(z, shift = -1)
                z = z + helpler
            endif
        enddo

        forall(i=0:n-1) z(i) = z(i)/(i+1)

        integral_A = sum(z)

    end function integral_A

    subroutine Inter_adams(X)
    real(8), dimension(0:, 1:), intent(out) :: X
    real(8), dimension(0:size(X)/ndim-1) :: T
    real(8), dimension(-1:n_inter_adams-2) :: B
    integer(4) :: i
    character(6) :: file
    
        T = 0; X(0,:) = X_started
        do i = 0, n_inter_adams-2
            X(i+1,:) = rk(X(i,:), t(i))
            T(i+1) = h*(i+1)
        enddo
        
        B = B_adams(n_inter_adams,(/(i,i=-1,n_inter_adams-2)/))
        
        do i = n_inter_adams-2, size(x)/ndim - 2
            call ia(X, T, B, i)
        enddo
        
        file = 'ia.dat'
        call Write_to_file(file, T, X)
        
    end subroutine Inter_adams
        
    subroutine ia(X, T, B, i)
    use Newton_module
    real(8), dimension(0:,1:) :: X
    real(8), dimension(0:) :: T
    real(8), dimension(-1:) :: B
    real(8), dimension(1:ndim) :: sum
    integer(4) :: i, j
        
        T(i+1) = h*(i+1)
        sum = 0
        do j = 0, n_inter_adams-2
            sum = sum + b(j)*f(t(i-j),X(i-j,:))
        enddo
        call newton(X(i,:), X(i+1,:), f_for_newton)
        
    contains

        function f_for_newton(z)
        real(8), dimension(1:), intent(in) :: z
        real(8), dimension(1:size(z)) :: f_for_newton

            f_for_newton = X(i,:) + h*sum - z + h*B(-1)*f(T(i+1),z)

        end function f_for_newton
    end subroutine ia
    
    elemental function B_adams(n,j)
    integer(4), intent(in) :: n, j
    integer(4) :: i
    real(8) :: B_adams

        B_adams = (-1.0)**(j+1) / product( (/(i,i=1,j+1)/))/product((/(i,i=1,n-2-j)/) )  * integral_B(n, j)

    end function B_adams
    
    pure function integral_B(n, j)
    integer(4), intent(in) :: n, j
    integer(4) :: a,i
    real(8), dimension(-1:n-2) :: z, helpler
    real(8) :: integral_B

        z = 0; z(-1) = 1;
        do a=-1, n-2
            if (a .ne. j) then
                helpler = z*a
                z = eoshift(z, shift = -1)
                z = z + helpler
            endif
        enddo

        forall(i=-1:n-2) z(i) = z(i)/(i+2)

        integral_B = sum(z)

    end function integral_B
    
end module ODU_solver    
