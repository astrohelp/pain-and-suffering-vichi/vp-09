module params

    implicit none
    
    integer(4), parameter :: ndim = 2
    integer(4) :: i
    real(8), parameter :: interval_end = 4.0
    real(8), dimension(ndim), parameter :: X_started = (/(1, i=1, ndim)/)
    real(8), parameter :: h = 0.01
    integer(4), parameter :: n_extr_adams = 4
    integer(4), parameter :: n_inter_adams = 4
    
    contains
    
    pure function f(t,X) result(Y)
    real(8), dimension(1:), intent(in) :: X
    real(8), intent(in) :: t
    real(8), dimension(1:size(X)) :: Y
    integer(4) :: i
        
        Y = 0
        Y(1) = sin(X(1)) - 0.42**(t)

    end function f
    
end module params