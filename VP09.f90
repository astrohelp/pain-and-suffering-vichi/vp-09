﻿program VP09

    use params
    use ODU_solver
    implicit none

    real(8), dimension(0:interval_end/h + 1, 1:ndim) :: X
    character(2) :: method

    call getarg(1, method)

    method = 'ia'
    select case(method)
        case('rk')
            open(10, file = 'rk.dat')
                call RungeKutta(X)
        case('ea')
            open(10, file = 'ea.dat')
                call Extr_adams(X)
        case('ia')
            open(10, file = 'ia.dat')
                call Inter_adams(X)
        case default
            stop 'Пожалуйста, выберете метод: rk для метода &
                Рунге-Кутта, ea для экстраполяционного метода &
                Адамса или ia для интерполяционного.'
    end select
    close(10)
end program VP09
