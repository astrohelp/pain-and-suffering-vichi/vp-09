﻿module Newton_module
    use vp04
    implicit none
    
    
    real(8), parameter :: accuracy=2e-8
    integer(4), parameter :: maxIterrations = 1000
    
    contains
    
    subroutine Fill(X0)
    real(8), dimension(1:) :: X0
   
    call RANDOM_NUMBER(X0)
    end subroutine Fill
    
    function AwesomeFunction(X) result(inputFunction)
    real(8), dimension(1:), intent(in) :: X
    real(8), dimension(1:size(X)) :: inputFunction
    integer(4) :: i, n
    
    n=size(X)

    do i=1,n
        inputFunction(i)=X(i)**i
    end do
    end function AwesomeFunction
    
    subroutine Newton(X0, X, AwesomeFunction)
    real(8), dimension(1:), intent(in) :: X0
    real(8), dimension(1:size(X0)), intent(out) :: X
    real(8), dimension(1:size(X0)) :: newX
    real(8), dimension(1:size(X), 1:size(X)) :: df
    real(8), dimension(1:size(X)+1, 1:size(X)) :: Matrix
    integer(4) :: i, n
    interface
        function AwesomeFunction(X) 
        real(8), dimension(1:), intent(in) :: X
        real(8), dimension(1:size(X)) :: AwesomeFunction
        end function AwesomeFunction
    end interface
    
    n=size(X0)
    i=0
    X=X0; newX=X+1.0;
    
    do while (sum(abs(newX-X))>accuracy .AND. i<maxIterrations)
        X=newX
        call derivative(X,df,AwesomeFunction)
        Matrix(1:n,1:n)=-df(1:n,1:n)
        Matrix(n+1,1:n)= AwesomeFunction(X) !тут ищем newX-X
        call choise(Matrix,newX,n)
        newX=newX+X !восстанавливаем newX
        i=i+1
    enddo
    X=newX
    end subroutine Newton
    
    subroutine derivative(X0,df,AwesomeFunction) !самое ленивое дифференцирование :)
    real(8), dimension(1:), intent(in) :: X0
    real(8), dimension(1:size(X0), 1:size(X0)), intent(out) :: df
    real(8), dimension(1:size(X0)) :: X
    real(8) :: eps=2e-8
    integer(8) :: i, n
    interface
        function AwesomeFunction(X) 
        real(8), dimension(1:), intent(in) :: X
        real(8), dimension(1:size(X)) :: AwesomeFunction
        end function AwesomeFunction
    end interface
    
    n=size(X0)
    
    do i=1,n
        X=X0
        X(i)=X0(i)+eps
        df(i,1:n)=(AwesomeFunction(X)-AwesomeFunction(X0))/eps
    enddo
    end subroutine derivative
end module Newton_module